using VocabularyTrainer.Model;

namespace VocabularyTrainer.SqlStorage.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<VocabularyTrainer.Model.CardData>
    {
        public Configuration()
        {
	        AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(VocabularyTrainer.Model.CardData context)
        {
			foreach (var cardset in CardDataSampleBuilder.Build(2, 5, "English"))
			{
				context.Cardsets.AddOrUpdate(cardset);
			}
		}
    }
}
