namespace VocabularyTrainer.SqlStorage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Term = c.String(),
                        CorrectTranslation = c.String(),
                        IncorrectCorrectTranslation1 = c.String(),
                        IncorrectCorrectTranslation2 = c.String(),
                        IncorrectCorrectTranslation3 = c.String(),
                        CardsetId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cardsets", t => t.CardsetId, cascadeDelete: true)
                .Index(t => t.CardsetId);
            
            CreateTable(
                "dbo.Cardsets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Code = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cards", "CardsetId", "dbo.Cardsets");
            DropIndex("dbo.Cards", new[] { "CardsetId" });
            DropTable("dbo.Cardsets");
            DropTable("dbo.Cards");
        }
    }
}
