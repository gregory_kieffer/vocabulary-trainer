﻿using System.Data.Entity;

namespace VocabularyTrainer.Model
{
	public class CardData : DbContext
	{
		
		public CardData() : base("CardData")
		{
		}

		public virtual DbSet<Card> Cards { get; set; }
		public virtual DbSet<Cardset> Cardsets { get; set; }
	}
}