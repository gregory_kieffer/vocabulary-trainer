﻿using System.Windows;
using VocabularyTrainer.Engine;

namespace VocabularyTrainer.Views
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		public Game Game { get; set; }
	}
}