﻿using System.Windows;
using System.Windows.Controls;

namespace VocabularyTrainer.Views
{
	public partial class AppPanel : ContentControl
	{
		public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof (object),
			typeof (AppPanel), new UIPropertyMetadata(null));

		public AppPanel()
		{
			InitializeComponent();
		}

		public object Title
		{
			get { return GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}
	}
}