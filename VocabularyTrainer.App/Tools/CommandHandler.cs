using System;
using System.Windows.Input;

namespace VocabularyTrainer.Tools
{
	public class CommandHandler<T> : ICommand where T : class
	{
		private readonly Action<T> _Action;
		private readonly bool _CanExecute;

		public CommandHandler(Action<T> action, bool canExecute)
		{
			_Action = action;
			_CanExecute = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			return _CanExecute;
		}

		public event EventHandler CanExecuteChanged;

		public void Execute(object parameter)
		{
			_Action(parameter as T);
		}
	}
}