using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using VocabularyTrainer.Model;
using VocabularyTrainer.ViewModels;

namespace VocabularyTrainer.Providers
{
	public abstract class CardsetProvider
	{
		public abstract string Name { get; }
		protected abstract IEnumerable<Cardset> Load();

		public void Populate(ObservableCollection<CardsetViewModel> collection, ICommand startCommand)
		{
			var data = Load();
			if (data == null || data.Count() == 0) return;
			foreach (var cardset in data)
			{
				collection.Add(new CardsetViewModel(cardset, startCommand));
			}
		}

		private static List<CardsetProvider> _Providers=new List<CardsetProvider>() ;

		static CardsetProvider()
		{

			_Providers.Add(new DatabaseCardsetProvider());
			_Providers.Add(new GeneratedCardsetProvider());
			_Providers.Add(new JsonCardsetProvider());
		}

		public static CardsetProvider Get(string name)
		{
			return _Providers.FirstOrDefault(p => string.Compare(p.Name,name,StringComparison.InvariantCultureIgnoreCase)==0);
		}
	}
}