using System.Collections.Generic;
using System.Linq;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Providers
{
	public class DatabaseCardsetProvider : CardsetProvider
	{
		public override string Name { get { return "Database"; } }

		protected override IEnumerable<Cardset> Load()
		{
			using (var db = new CardData())
			{
				return db.Cardsets.Include("Cards").ToList();

			}
		}
	}
}