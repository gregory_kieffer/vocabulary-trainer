using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VocabularyTrainer.Model;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace VocabularyTrainer.Providers
{
	public class JsonCardsetProvider : CardsetProvider
	{
		public override string Name
		{
			get { return "json"; }
		}

		protected override IEnumerable<Cardset> Load()
		{
			var dialog = new OpenFileDialog();
			dialog.Multiselect = false;
			dialog.CheckFileExists = true;
			dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
			if (dialog.ShowDialog().GetValueOrDefault(true))
			{
				var filename = dialog.FileName;
				try
				{
					return CardsetManager.Load(filename);
				}
				catch
				{
					MessageBox.Show($"Failed to load file {filename}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
			return null;
		}
	}
}