using System.Collections.Generic;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Providers
{
	public class GeneratedCardsetProvider : CardsetProvider
	{
		public override string Name { get { return "Generated"; } }

		protected override IEnumerable<Cardset> Load()
		{
			return CardDataSampleBuilder.Build(10, null, "English");
		}
	}
}