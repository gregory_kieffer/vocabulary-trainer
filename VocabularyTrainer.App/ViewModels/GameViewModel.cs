﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Windows.Input;
using VocabularyTrainer.Engine;
using VocabularyTrainer.Model;
using VocabularyTrainer.Providers;
using VocabularyTrainer.Tools;

namespace VocabularyTrainer.ViewModels
{
	public class GameViewModel : ViewModelBase
	{
		private PlayableCard _ActiveCard;
		private bool _CardsetSelectorVisible;
		private bool _Congratulate;
		private Answer _CorrectAnswer;
		private bool _Flagelate;
		private Game _Game;
		private string _GameTitle;
		private ObservableCollection<CardsetViewModel> _LoadedCardsets;
		private IEnumerable<Answer> _PossibleAnswers;
		private int? _SecondsLeft;
		private Answer _SelectedAnswer;
		private bool _ShowCorrectAnswer;
		private bool _ShowSecondsLeft;

		public GameViewModel()
		{
			StartCommand = new CommandHandler<Cardset>(StartCommand_Execute, true);
			ImportCommand = new CommandHandler<string>(ImportCommand_Execute, true);

			Game = null;
			CardsetSelectorVisible = true;
			LoadedCardsets = new ObservableCollection<CardsetViewModel>();
			if (DesignMode)
			{
				LoadCardsets("generated");
			}
		}

		public ICommand ImportCommand { get; private set; }
		public ICommand StartCommand { get; }

		public IEnumerable<Answer> PossibleAnswers
		{
			get { return _PossibleAnswers; }
			set { SetProperty(ref _PossibleAnswers, value); }
		}

		public string GameTitle
		{
			get { return _GameTitle; }
			set { SetProperty(ref _GameTitle, value); }
		}

		public Game Game
		{
			get { return _Game; }
			set
			{
				SetProperty(ref _Game, value);
				GameTitle = _Game != null ? Game.Cardset.Description : "No card set loaded";
			}
		}

		public PlayableCard ActiveCard
		{
			get { return _ActiveCard; }
			set { SetProperty(ref _ActiveCard, value); }
		}

		public Answer SelectedAnswer
		{
			get { return _SelectedAnswer; }
			set
			{
				Game.Answer(value);
				SetProperty(ref _SelectedAnswer, value);
			}
		}

		public Answer CorrectAnswer
		{
			get { return _CorrectAnswer; }
			set { SetProperty(ref _CorrectAnswer, value); }
		}

		public int? SecondsLeft
		{
			get { return _SecondsLeft; }
			set { SetProperty(ref _SecondsLeft, value); }
		}

		public bool ShowSecondsLeft
		{
			get { return _ShowSecondsLeft; }
			set { SetProperty(ref _ShowSecondsLeft, value); }
		}

		public bool CardsetSelectorVisible
		{
			get { return _CardsetSelectorVisible; }
			set { SetProperty(ref _CardsetSelectorVisible, value); }
		}

		public ObservableCollection<CardsetViewModel> LoadedCardsets

		{
			get { return _LoadedCardsets; }
			set { SetProperty(ref _LoadedCardsets, value); }
		}

		public DateTime CardStartTime { get; set; }

		public bool ShowCorrectAnswer
		{
			get { return _ShowCorrectAnswer; }
			set { SetProperty(ref _ShowCorrectAnswer, value); }
		}

		public bool Congratulate
		{
			get { return _Congratulate; }
			set { SetProperty(ref _Congratulate, value); }
		}

		public bool Flagelate
		{
			get { return _Flagelate; }
			set { SetProperty(ref _Flagelate, value); }
		}

		public void LoadCardsets(string providerName)
		{
			var provider = CardsetProvider.Get(providerName);
			provider?.Populate(LoadedCardsets, StartCommand);
		}

		private void ImportCommand_Execute(string providerName)
		{
			Game?.Stop();
			Game = null;
			LoadCardsets(providerName);
			CardsetSelectorVisible = true;
		}

		private void StartCommand_Execute(Cardset cardset)
		{
			if (cardset != null) Start(cardset);
		}

		public void Start(Cardset cardset)
		{
			CardsetSelectorVisible = false;
			Game = Game.Create(cardset);
			Game.CardStart += Game_CardStart;
			Game.CardStop += Game_CardStop;
			Game.TimeLeftChanged += Game_TimeLeftChanged;
			Game.ResultPublished += Game_ResultPublished;
			Game.CardsetComplete += Game_CardsetComplete;
			Game.Start();
		}

		private void Game_CardsetComplete()
		{
			MessageBox.Show("Card set complete, congratulations");
			Game.Dispose();
			Game = null;
		}

		private void Game_ResultPublished(PlayableCard card, bool success, Answer correctAnswer)
		{
			Flagelate = !success;
			Congratulate = success;
			CorrectAnswer = correctAnswer;
			ShowCorrectAnswer = true;
		}

		private void Game_TimeLeftChanged(int? secondsLeft)
		{
			SecondsLeft = secondsLeft;
			ShowSecondsLeft = SecondsLeft.HasValue;
		}

		private void Game_CardStop(PlayableCard card)
		{
			ActiveCard = null;
		}

		private void Game_CardStart(PlayableCard card)
		{
			PossibleAnswers = card.BuildAnswers();
			ActiveCard = card;

			ShowCorrectAnswer = false;
			Flagelate = false;
			Congratulate = false;
			CorrectAnswer = null;
		}
	}
}