using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace VocabularyTrainer.ViewModels
{
	public class ViewModelBase : INotifyPropertyChanged
	{
		static ViewModelBase()
		{
			var dependencyObject = new DependencyObject();
			DesignMode = DesignerProperties.GetIsInDesignMode(dependencyObject);
		}

		public static bool DesignMode { get; private set; }
		public event PropertyChangedEventHandler PropertyChanged;

		protected void NotifyPropertyChanged(string propertyName = null)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
		{
			if (Equals(storage, value))
			{
				return false;
			}

			storage = value;
			NotifyPropertyChanged(propertyName);
			return true;
		}
	}
}