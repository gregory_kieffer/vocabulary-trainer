using System.Windows.Input;
using VocabularyTrainer.Model;
using VocabularyTrainer.Tools;

namespace VocabularyTrainer.ViewModels
{
	public class CardsetViewModel
	{
		private readonly ICommand _StartGameCommand;

		public CardsetViewModel(Cardset cardSet, ICommand startGameCommand)
		{
			Cardset = cardSet;

			_StartGameCommand = startGameCommand;
			SelectCommand = new CommandHandler<object>(Select, true);
		}

		public Cardset Cardset { get; set; }
		public ICommand SelectCommand { get; private set; }

		private void Select(object o)
		{
			_StartGameCommand.Execute(Cardset);
		}
	}
}