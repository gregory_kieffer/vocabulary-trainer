﻿using System.Runtime.Serialization;

namespace VocabularyTrainer.Model
{
	[DataContract]
	public class Card
	{
		[DataMember]
		public int Id { get; set; }

		[DataMember]
		public string Term { get; set; }

		[DataMember]
		public string CorrectTranslation { get; set; }

		[DataMember]
		public string IncorrectCorrectTranslation1 { get; set; }

		[DataMember]
		public string IncorrectCorrectTranslation2 { get; set; }

		[DataMember]
		public string IncorrectCorrectTranslation3 { get; set; }

		public virtual Cardset Cardset { get; set; }
		public int CardsetId { get; set; }
	}
}