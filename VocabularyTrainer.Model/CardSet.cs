using System.Collections.Generic;
using System.Runtime.Serialization;

namespace VocabularyTrainer.Model
{
	[DataContract]
	public class Cardset
	{
		public Cardset()
		{
			Cards = new List<Card>();
		}

		[DataMember]
		public int Id { get; set; }

		[DataMember]
		public string Description { get; set; }

		[DataMember]
		public string Code { get; set; }

		[DataMember]
		public ICollection<Card> Cards { get; set; }
	}
}