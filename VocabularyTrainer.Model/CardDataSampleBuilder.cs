﻿using System;
using System.Collections.Generic;

namespace VocabularyTrainer.Model
{
	public static class CardDataSampleBuilder
	{
		public static IEnumerable<Cardset> Build(int cardSetCount, int? cardCount, string languageName)
		{
			var collection = new List<Cardset>();
			var random = new Random();

			for (var i = 1; i <= cardSetCount; i++)
			{
				var cardSet = new Cardset
				{
					Code = $"CS-L{i}",
					Description = $"{languageName} Level {i}"
				};
				if (!cardCount.HasValue)
				{
					cardCount = random.Next(2, 20); // a set is composed of at least two cards...    
				}

				for (var ii = 1; ii <= cardCount; ii++)
				{
					cardSet.Cards.Add(new Card
					{
						Cardset = cardSet,
						Term = $"term{i}.{ii}",
						CorrectTranslation = "correct",
						IncorrectCorrectTranslation1 = "incorrect1",
						IncorrectCorrectTranslation2 = "incorrect2",
						IncorrectCorrectTranslation3 = "incorrect3"
					});
				}
				collection.Add(cardSet);
			}
			return collection;
		}
	}
}