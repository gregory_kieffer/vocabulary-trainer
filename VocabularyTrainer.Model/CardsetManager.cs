using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;

namespace VocabularyTrainer.Model
{
	public static class CardsetManager
	{
		private static DataContractJsonSerializer CreateSerializer()
		{
			return new DataContractJsonSerializer(typeof (Cardset[]), new[] {typeof (Card)});
		}

		public static void Save(this IEnumerable<Cardset> list, string filename)
		{
			if (File.Exists(filename)) File.Delete(filename);


			using (var stream = File.OpenWrite(filename))
			{
				var serializer = CreateSerializer();
				serializer.WriteObject(stream, list.ToArray());
			}
		}

		public static IEnumerable<Cardset> Load(string filename)
		{
			using (var stream = File.OpenRead(filename))
			{
				var serializer = CreateSerializer();
				var array = serializer.ReadObject(stream) as Cardset[];
				if (array != null)
				{
					foreach (var cardSet in array)
					{
						yield return cardSet;
					}
				}
			}
		}
	}
}