﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VocabularyTrainer.Engine;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Tests
{
	[TestClass, ExcludeFromCodeCoverage]
	public class EngineTests
	{
		[TestMethod]
		public void StartGame()
		{
			var sample = CardDataSampleBuilder.Build(1, 2, "English").First();


			using (var game = Game.Create(sample, 10, 1, 1))
			{
				game.ResultPublished += (playableCard, success, answer) => { if (!game.Complete) Assert.IsNotNull(answer); };

				game.TimeLeftChanged += left => Assert.IsTrue(left.GetValueOrDefault(0) < 6);
				game.CardStart += Assert.IsNotNull;
				game.CardStop += Assert.IsNotNull;

				game.Start();
				Assert.AreEqual(0, game.SecondsLeft); // at the given test speed, secondsLeft will always be 0
				Assert.IsNotNull(game.Cardset);
				Assert.IsTrue(game.Cardset.Cards.Count > 1);
				Assert.IsFalse(game.Complete);

				Assert.IsNotNull(game.CurrentCard);

				var card = game.CurrentCard;


				var possibleAnswers = card.BuildAnswers().ToList();

				Assert.IsNotNull(possibleAnswers);
				Assert.AreEqual(4, possibleAnswers.Count); // 3 incorrect + 1 correct
				Assert.IsFalse(card.Complete);

				Assert.AreEqual(0, card.Answers.Count());

				// select any incorrect answer
				var incorrectAnswer = possibleAnswers.First(answer => answer.Correct == false);
				var correctAnswer = card.CorrectAnswer;


				Assert.IsNotNull(incorrectAnswer.Card);

				card.AddAnswer(incorrectAnswer);
				Assert.IsFalse(card.Complete);

				card.AddAnswer(correctAnswer);
				Assert.IsFalse(card.Complete);
				card.AddAnswer(correctAnswer);
				Assert.IsFalse(card.Complete);
				card.AddAnswer(correctAnswer);
				Assert.IsTrue(card.Complete);


				game.Advance();
				card = game.CurrentCard;

				game.Answer(card.CorrectAnswer);

				Assert.IsFalse(game.Complete);
				game.Stop();
			}
		}
	}
}