﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Tests
{
	[TestClass, ExcludeFromCodeCoverage]
	public class CardDataTests
	{
		[TestMethod]
		public void CheckDatabaseConnection()
		{
			// we expect the database to bee seeded after opening
			using (var db = new CardData())
			{
				Assert.IsTrue(db.Cardsets.Any());
			}
		}

		[TestMethod]
		public void SaveSampleCardset()
		{
			var sample = CardDataSampleBuilder.Build(10, null, "English");
			var tempFilename = Path.GetTempFileName();
			sample.Save(tempFilename);
			var sampleReloaded = new List<Cardset>();
			sampleReloaded.AddRange(CardsetManager.Load(tempFilename));
			Assert.IsTrue(sampleReloaded.Any());
			Assert.AreEqual(sample.Count(), sampleReloaded.Count);

			foreach (var cardSetReloaded in sampleReloaded)
			{
				var cardset = sample.First(c => c.Code == cardSetReloaded.Code);
				Assert.IsNotNull(cardset);
				Assert.AreEqual(cardset.Cards.Count, cardSetReloaded.Cards.Count);
			}
		}

		[TestMethod]
		public void BuilsSampleCardset()
		{
			var sample = CardDataSampleBuilder.Build(1, 2, "English");

			Assert.IsTrue(sample.Any());
			foreach (var cardSet in sample)
			{
				Assert.IsTrue(cardSet.Cards.Count == 2);
			}
		}
	}
}