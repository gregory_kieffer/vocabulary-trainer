namespace VocabularyTrainer.Engine
{
	public delegate void ResultDelegate(PlayableCard card, bool success, Answer correctAnswer);
}