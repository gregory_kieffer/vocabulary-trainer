﻿using System;
using System.Collections.Generic;
using System.Linq;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Engine
{
	public class PlayableCard
	{
		private readonly List<Answer> _Answers = new List<Answer>();

		public PlayableCard(Card data)
		{
			Data = data;
		}

		public IEnumerable<Answer> Answers => _Answers; // to avoid cheating, should be cloned
		public Card Data { get; set; }

		public bool Complete
		{
			get
			{
				var correctAnswers = _Answers.Where(a => a != null && a.Correct);
				return correctAnswers.Count() >= 3;
			}
		}

		public Answer CorrectAnswer => new Answer(Data, true, Data.CorrectTranslation);

		public IEnumerable<Answer> BuildAnswers()
		{
			var answers = new List<Answer>(new[]
			{
				new Answer(Data, true, Data.CorrectTranslation),
				new Answer(Data, false, Data.IncorrectCorrectTranslation1),
				new Answer(Data, false, Data.IncorrectCorrectTranslation2),
				new Answer(Data, false, Data.IncorrectCorrectTranslation3)
			});


			var random = new Random();
			while (answers.Any())
			{
				var index = random.Next(answers.Count);
				yield return answers[index];
				answers.RemoveAt(index);
			}
		}

		public void AddAnswer(Answer answer)
		{
			if (answer.Card == Data) _Answers.Add(answer);
		}
	}
}