﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using VocabularyTrainer.Model;

namespace VocabularyTrainer.Engine
{
	public class Game : IDisposable
	{
		private readonly int _CardTimeoutMilliseconds = 5000;
		private readonly int _FeedbackFrequencyMilliseconds = 100;
		private readonly int _ResultTimeoutMilliseconds = 3000;
		private DateTime _CardStartTime;
		private Timer _CardTimer;
		private Timer _FeedbackTimer;
		private Random _Random;
		private Timer _ResultTimer;

		public Game(int cardTimeoutMilliseconds, int resultTimeoutMilliseconds, int feedbackFrequencyMilliseconds)
		{
			_CardTimeoutMilliseconds = cardTimeoutMilliseconds;
			_ResultTimeoutMilliseconds = resultTimeoutMilliseconds;
			_FeedbackFrequencyMilliseconds = feedbackFrequencyMilliseconds;
		}

		public int SecondsLeft { get; private set; }
		public Answer LastAnswer { get; set; }
		public Cardset Cardset { get; set; }
		public List<PlayableCard> PlayableCards { get; set; }
		public PlayableCard CurrentCard { get; private set; }
		public bool Complete { get; private set; }

		public void Dispose()
		{
			CurrentCard = null;
			_CardTimer?.Dispose();
			_ResultTimer?.Dispose();
			_FeedbackTimer?.Dispose();
		}

		public event CardDelegate CardStart;
		public event CardDelegate CardStop;
		public event CardAnswerDelegate AnswerSelected;
		public event ResultDelegate ResultPublished;
		public event TimeLeftDelegate TimeLeftChanged;
		public event CardsetCompleteDelegate CardsetComplete;

		public static Game Create(Cardset cardSet, int cardTimeoutMilliseconds = 5000, int resultTimeoutMilliseconds = 3000,
			int feedbackFrequencyMilliseconds = 100)
		{
			var game = new Game(cardTimeoutMilliseconds, resultTimeoutMilliseconds, feedbackFrequencyMilliseconds)
			{
				Cardset = cardSet
			};

			game.Prepare();
			return game;
		}

		private void Prepare()
		{
			// create empty responses
			PlayableCards = (from c in Cardset.Cards select new PlayableCard(c)).ToList();
			_Random = new Random();

			// choose a card already
			Advance();
		}

		private PlayableCard SelectNextPlayableCard()
		{
			// build an array of cards that are not complete yey (with less than 3 correct answers)
			var availableCards = (from r in PlayableCards where !r.Complete select r).ToArray();

			if (availableCards.Length == 0) return null;

			// choose a random card from those
			var index = _Random.Next(availableCards.Length);
			return availableCards[index];
		}

		public void Start()
		{
			SecondsLeft = 0;
			_FeedbackTimer = new Timer(FeedbackTimerCallback, null, _FeedbackFrequencyMilliseconds,
				_FeedbackFrequencyMilliseconds);
			Complete = false;
			Advance();
		}

		private void CardTimerCallback(object state)
		{
			_CardTimer?.Dispose();
			if (CurrentCard != null)
			{
				if (LastAnswer != null)
				{
					CurrentCard?.AddAnswer(LastAnswer);
					ProvideResult();
				}
				else
				{
					OnCardStop(CurrentCard);
					Advance();
				}
			}
			else
			{
				Advance();
			}
		}

		private void ProvideResult()
		{
			if (LastAnswer == null) return;

			_ResultTimer = new Timer(ResultTimerCallback, null, _ResultTimeoutMilliseconds, -1);

			OnResultPublished(CurrentCard, LastAnswer.Correct, LastAnswer.Correct ? LastAnswer : CurrentCard.CorrectAnswer);
		}

		private void ResultTimerCallback(object state)
		{
			_ResultTimer?.Dispose();
			OnCardStop(CurrentCard);
			Advance();
		}

		public void Answer(Answer answer)
		{
			LastAnswer = answer;
		}

		public void Advance()
		{
			CurrentCard = SelectNextPlayableCard();

			if (CurrentCard != null)
			{
				_CardTimer = new Timer(CardTimerCallback, null, _CardTimeoutMilliseconds, -1);
				_CardStartTime = DateTime.Now;

				OnCardStart(CurrentCard);
			}
			else
			{
				Complete = true;
				Stop();
				OnCardsetComplete();
			}
		}

		private void FeedbackTimerCallback(object state)
		{
			var secondsEllapsed = (DateTime.Now - _CardStartTime).Seconds;
			var newSecondsLeft = Math.Max(TimeSpan.FromMilliseconds(_CardTimeoutMilliseconds).Seconds - secondsEllapsed, 0);
			if (newSecondsLeft != SecondsLeft)
			{
				SecondsLeft = newSecondsLeft;
				OnTimeLeftChanged(SecondsLeft);
			}
		}

		public void Stop()
		{
			CurrentCard = null;
			_CardTimer?.Dispose();
			_ResultTimer?.Dispose();

			_FeedbackTimer?.Dispose();
			OnTimeLeftChanged(null);
		}

		protected virtual void OnCardStart(PlayableCard card)
		{
			CardStart?.Invoke(card);
		}

		protected virtual void OnCardStop(PlayableCard card)
		{
			CardStop?.Invoke(card);
		}

		protected virtual void OnResultPublished(PlayableCard card, bool success, Answer correctanswer)
		{
			ResultPublished?.Invoke(card, success, correctanswer);
		}

		protected virtual void OnTimeLeftChanged(int? secondsleft)
		{
			TimeLeftChanged?.Invoke(secondsleft);
		}

		protected virtual void OnCardsetComplete()
		{
			CardsetComplete?.Invoke();
		}
	}
}