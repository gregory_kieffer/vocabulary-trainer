﻿using VocabularyTrainer.Model;

namespace VocabularyTrainer.Engine
{
	public class Answer
	{
		public Answer(Card card, bool correct, string text)
		{
			Card = card;
			Correct = correct;
			Text = text;
		}

		public Card Card { get; private set; }
		public bool Correct { get; private set; }
		public string Text { get; set; }
	}
}