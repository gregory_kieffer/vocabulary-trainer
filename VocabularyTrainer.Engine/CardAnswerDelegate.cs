namespace VocabularyTrainer.Engine
{
	public delegate void CardAnswerDelegate(PlayableCard card, Answer answer);
}